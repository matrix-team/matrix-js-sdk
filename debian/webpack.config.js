'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

    resolve: {
        modules: [
            '/usr/lib/nodejs',
            '/usr/share/nodejs',
            '.',
        ],
    },

    resolveLoader: {
        modules: ['/usr/lib/nodejs', '/usr/share/nodejs'],
    },

    node: {
        fs: 'empty',
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    // although we run our source through babel already, we need
                    // this for the dependencies
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-es2015'],
                    },
                },
            },
        ],
    },

    devtool: 'source-map',
};

module.exports = config;
